#-------------------------------------------------
#
# Project created by QtCreator 2014-05-03T15:29:59
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageViewer
TEMPLATE = app

LIBS += -lX11 -lXrandr

SOURCES += main.cpp\
        mainwindow.cpp \
    imageinfo.cpp \
    imagecontainer.cpp \
    image.cpp \
    imagelist.cpp \
    xfcewallpapersetter.cpp

HEADERS  += mainwindow.h \
    imageinfo.h \
    imagecontainer.h \
    image.h \
    imagelistitem.h \
    imagelist.h \
    xfcewallpapersetter.h

FORMS    += \
    imageinfo.ui \
    mainwindow.ui

RESOURCES += \
    icons.qrc
