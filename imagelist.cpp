#include "imagelist.h"

ImageList::ImageList(QWidget *parent) :
    QListWidget(parent)
{
    this->setItemDelegate(new ImageListItem(this));
    this->setVisible(false);
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    this->setSelectionMode(QAbstractItemView::ExtendedSelection);
    this->setFrameStyle(QFrame::NoFrame);
    this->resize(250, 250);

    this->sortDir = ILUpSort;
    this->sortType = ILNameSort;

    sortProcess = false;
    loadProcess = false;

    this->loadCircleImage.load("://images/load.png");

}

void ImageList::setDirectory(QDir directory, QString imageName)
{
    this->directory = directory;
    QStringList filters;
    filters << "*.jpg" << "*.png";
    this->directory.setNameFilters(filters);


    this->clear();
    QThreadPool::globalInstance()->start(new LoadImagesTask(this, imageName));
}

void ImageList::setCurrentImageByName(QString imageName)
{

    for(int i = 0; i < this->count(); i++)
    {
        if(this->item(i)->text() == imageName)
        {
            this->setCurrentImageByIndex(i);
            break;
        }
    }
}

void ImageList::setCurrentImageByIndex(int index)
{
    this->setCurrentRow(index);
    if(this->isVisible())
        this->scrollToItem(this->currentItem());
}

QString ImageList::currentImage()
{
    return this->currentItem()->data(Qt::UserRole+1).toString();
}

void ImageList::removeSelectedItems()
{
    int lastRemovedRow;
    QList<QListWidgetItem *> selectedItemsList = this->selectedItems();
    for(int i = 0; i < selectedItemsList.size(); i++)
    {
        if(QFile::remove(selectedItemsList.value(i)->data(Qt::UserRole+1).toString()))
        {
            lastRemovedRow = this->row(selectedItemsList.value(i));
            this->takeItem(lastRemovedRow);
        }
    }
    if(lastRemovedRow > this->count()-1)
    {
        lastRemovedRow = this->count()-1;
    }
    this->setCurrentImageByIndex(lastRemovedRow);
}

void ImageList::setSortType(ILSortType type)
{
    this->sortType = type;
    if(QThreadPool::globalInstance()->tryStart(new SortTask(this)))
    {
        qDebug() << "start sort";
    }
}

void ImageList::setSortDir(ILSortDir dir)
{
    this->sortDir = dir;
}

void ImageList::updateSort()
{
    QList<QListWidgetItem *> it;

    while(this->count()>0)
        it.push_back(this->takeItem(0));

    qSort(it.begin(), it.end(), PtrLess(this->sortType, this->sortDir));

    for(int i = 0; i < it.size(); ++i)
        this->addItem(it[i]);
}

void ImageList::paintEvent(QPaintEvent *e)
{
    QListWidget::paintEvent(e);


    if(this->loadProcess)
    {
        QPainter painter(this->viewport());
        painter.fillRect(this->rect(), QBrush(QColor(50,50,50)));
        painter.drawImage(QPoint(this->rect().width()/2-loadCircleImage.rect().width()/2,
                                 this->rect().height()/2-loadCircleImage.rect().height()/2), loadCircleImage);
    }

}

void ImageList::next()
{
    int next = this->currentRow() + 1;
    if(next < this->count())
    {
        this->setCurrentImageByIndex(next);
    }else{
        next = 0;
        this->setCurrentImageByIndex(next);
    }
}

void ImageList::previous()
{
    int next = this->currentRow() - 1;
    if(next > 0)
    {
        this->setCurrentImageByIndex(next);
    }else{
        next = this->count()-1;
        this->setCurrentImageByIndex(next);
    }
}

