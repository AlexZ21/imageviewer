#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QDesktopWidget *descWidget, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->descWidget = descWidget;

    image = new Image(this);


    imageContainer = new ImageContainer(this);

    imageInfo = new ImageInfo(this);
    imageInfo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    imageList = new ImageList(this);
    QObject::connect(imageList, SIGNAL(currentRowChanged(int)), this, SLOT(imageListRowChanged(int)));
    ui->verticalLayout->addWidget(imageList);

    this->createToolButtons();
    this->createActions();
    this->disableButtons();

    ui->scrollArea->setWidget(imageContainer);
    ui->scrollArea->setAlignment(Qt::AlignCenter);
    ui->scrollArea->setWidgetResizable(false);
    QPalette saBackgroundPallete = ui->scrollArea->palette();
    saBackgroundPallete.setColor(ui->scrollArea->backgroundRole(), QColor(40,40,40));
    ui->scrollArea->setPalette(saBackgroundPallete);

    this->setWindowTitle("Просмотр изображений");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createToolButtons()
{
    nextButton = new QToolButton(this);
    nextButton->setIcon(QIcon::fromTheme("go-next"));
    if(nextButton->icon().isNull())
    {
        nextButton->setIcon(QIcon("://images/go-next.png"));
    }
    QObject::connect(nextButton, SIGNAL(pressed()),imageList, SLOT(next()));

    prevButton = new QToolButton(this);
    prevButton->setIcon(QIcon::fromTheme("go-previous"));
    if(prevButton->icon().isNull())
    {
        prevButton->setIcon(QIcon("://images/go-previous.png"));
    }
    QObject::connect(prevButton, SIGNAL(pressed()),imageList, SLOT(previous()));

    fitToWindowButton = new QToolButton(this);
    fitToWindowButton->setIcon(QIcon::fromTheme("zoom-fit-best"));
    if(fitToWindowButton->icon().isNull())
    {
        fitToWindowButton->setIcon(QIcon("://images/zoom-fit-best.png"));
    }
    fitToWindowButton->setCheckable(true);
    fitToWindowButton->setChecked(true);
    QObject::connect(fitToWindowButton, SIGNAL(toggled(bool)),this, SLOT(fitToWindowButtonToggled(bool)));

    zoomOriginalButton = new QToolButton(this);
    zoomOriginalButton->setIcon(QIcon::fromTheme("zoom-original"));
    if(zoomOriginalButton->icon().isNull())
    {
        zoomOriginalButton->setIcon(QIcon("://images/zoom-original.png"));
    }
    QObject::connect(zoomOriginalButton, SIGNAL(clicked()),this, SLOT(zoomOriginalButtonPressed()));

    zoomInButton = new QToolButton(this);
    zoomInButton->setIcon(QIcon::fromTheme("zoom-in"));
    if(zoomInButton->icon().isNull())
    {
        zoomInButton->setIcon(QIcon("://images/zoom-in.png"));
    }
    QObject::connect(zoomInButton, SIGNAL(clicked()),this, SLOT(zoomInButtonPressed()));

    zoomOutButton = new QToolButton(this);
    zoomOutButton->setIcon(QIcon::fromTheme("zoom-out"));
    if(zoomOutButton->icon().isNull())
    {
        zoomOutButton->setIcon(QIcon("://images/zoom-out.png"));
    }
    QObject::connect(zoomOutButton, SIGNAL(clicked()),this, SLOT(zoomOutButtonPressed()));

    settingsButton = new QToolButton(this);
    settingsButton->setIcon(QIcon::fromTheme("document-properties"));
    if(settingsButton->icon().isNull())
    {
        settingsButton->setIcon(QIcon("://images/document-properties.png"));
    }
    settingsButton->setPopupMode(QToolButton::InstantPopup);
    settingsButton->setToolButtonStyle(Qt::ToolButtonFollowStyle);
    settingsButton->setStyleSheet("QToolButton::menu-indicator { image: none; }");


    ui->mainToolBar->addWidget(prevButton);
    ui->mainToolBar->addWidget(nextButton);
    ui->mainToolBar->addWidget(zoomInButton);
    ui->mainToolBar->addWidget(zoomOutButton);
    ui->mainToolBar->addWidget(fitToWindowButton);
    ui->mainToolBar->addWidget(zoomOriginalButton);
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addWidget(imageInfo);
    ui->mainToolBar->addWidget(settingsButton);
}

void MainWindow::createActions()
{
    QMenu *mainMenu = new QMenu(settingsButton);
    openImageAction = new QAction(tr("Открыть изображение"), settingsButton);
    QObject::connect(openImageAction, SIGNAL(triggered()),this, SLOT(openImageActionTriggered()));

    removeImageAction = new QAction(tr("Удалить изображение"), settingsButton);
    QObject::connect(removeImageAction, SIGNAL(triggered()),this, SLOT(removeImageActionTriggered()));

    setAsWallpaperXFCEAction = new QAction(tr("Установить как обои(XFCE)"), settingsButton);
    QObject::connect(setAsWallpaperXFCEAction, SIGNAL(triggered()),this, SLOT(setAsWallpaperXFCEActionTriggered()));

    showFileListAction = new QAction(tr("Показывать список изображений"), settingsButton);
    showFileListAction->setCheckable(true);
    showFileListAction->setChecked(false);
    QObject::connect(showFileListAction, SIGNAL(toggled(bool)),imageList, SLOT(setVisible(bool)));

    aboutAction = new QAction(tr("О программе"), settingsButton);
    QObject::connect(aboutAction, SIGNAL(triggered()),this, SLOT(aboutActionTriggered()));

    exitAction = new QAction(tr("Выход"), settingsButton);
    QObject::connect(exitAction, SIGNAL(triggered()),this, SLOT(close()));

    QMenu *setAsMenu = new QMenu(tr("Установить как обои"), settingsButton);
    setAsMenu->addAction(setAsWallpaperXFCEAction);

    QMenu *sortMenu = new QMenu(tr("Сортировка"), settingsButton);

    sortActionGroup = new QActionGroup(settingsButton);
    QObject::connect(sortActionGroup, SIGNAL(triggered(QAction*)),this, SLOT(sortActionGroupTriggered(QAction*)));
    directionSortActionGroup = new QActionGroup(settingsButton);
    QObject::connect(directionSortActionGroup, SIGNAL(triggered(QAction*)),this, SLOT(directionSortActionGroupTriggered(QAction*)));

    nameSortAction = new QAction(tr("По имени"), settingsButton);
    nameSortAction->setData(ILNameSort);
    nameSortAction->setCheckable(true);
    nameSortAction->setChecked(true);

    dateSortAction = new QAction(tr("По дате"), settingsButton);
    dateSortAction->setData(ILDateSort);
    dateSortAction->setCheckable(true);

    sizeSortAction = new QAction(tr("По размеру"), settingsButton);
    sizeSortAction->setData(ILSizeSort);
    sizeSortAction->setCheckable(true);

    typeSortAction = new QAction(tr("По типу"), settingsButton);
    typeSortAction->setData(ILTypeSort);
    typeSortAction->setCheckable(true);

    upSortAction = new QAction(tr("По возрастанию"), settingsButton);
    upSortAction->setCheckable(true);
    upSortAction->setChecked(true);

    downSortAction = new QAction(tr("По убыванию"), settingsButton);
    downSortAction->setCheckable(true);

    sortActionGroup->addAction(nameSortAction);
    sortActionGroup->addAction(dateSortAction);
    sortActionGroup->addAction(sizeSortAction);
    sortActionGroup->addAction(typeSortAction);

    directionSortActionGroup->addAction(upSortAction);
    directionSortActionGroup->addAction(downSortAction);

    sortMenu->addAction(nameSortAction);
    sortMenu->addAction(dateSortAction);
    sortMenu->addAction(sizeSortAction);
    sortMenu->addAction(typeSortAction);
    sortMenu->addSeparator();
    sortMenu->addAction(upSortAction);
    sortMenu->addAction(downSortAction);

    mainMenu->addAction(openImageAction);
    mainMenu->addAction(removeImageAction);
    mainMenu->addMenu(setAsMenu);
    mainMenu->addAction(showFileListAction);
    mainMenu->addMenu(sortMenu);
    mainMenu->addSeparator();
    mainMenu->addAction(aboutAction);
    mainMenu->addAction(exitAction);

    settingsButton->setMenu(mainMenu);
}


void MainWindow::openImage(QString filePath)
{
    if(image->load(filePath))
    {
        imageContainer->setImage(image->getImage());

        imageList->setDirectory(image->getDirectory(), image->getName());

        upateImageInfoWidget();
        this->enableButtons();
    }
}


float MainWindow::getScrollBarRatio(QScrollBar *scrollBar)
{
    float ratio = 0.5;
    if(scrollBar->maximum() > 0)
        ratio = (float)scrollBar->value()/scrollBar->maximum();
    return ratio;
}


void MainWindow::openImageActionTriggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Открыть изображение"),"",tr("Images (*.png *.xpm *.jpg)"));
    if(!fileName.isEmpty())
    {
        this->openImage(fileName);
    }

}

void MainWindow::removeImageActionTriggered()
{
    int result = QMessageBox::warning(this, tr("Удалить файл"),
                                      tr("Вы уверены, что навсегда.\n"
                                         "хотите удалить это изображение?"),
                                      QMessageBox::Yes | QMessageBox::Default,
                                      QMessageBox::No | QMessageBox::Escape);
    if (result == QMessageBox::Yes) {

        imageList->removeSelectedItems();
    }
    
}

void MainWindow::setAsWallpaperXFCEActionTriggered()
{
    XFCEWallpaperSetter setter(descWidget, this);
    setter.setAsWallpaper(image->getPath());
}

void MainWindow::aboutActionTriggered()
{
    QMessageBox::about(this, tr("О программе"), tr("Автор: Александр Журавлев, 2014"));
}

void MainWindow::sortActionGroupTriggered(QAction *action)
{
    imageList->setSortType((ILSortType)action->data().toInt());
}

void MainWindow::directionSortActionGroupTriggered(QAction *action)
{
    imageList->setSortDir((ILSortDir)action->data().toInt());
}

void MainWindow::fitToWindowButtonToggled(bool mode)
{
    imageContainer->setFitToParent(mode);
}

void MainWindow::zoomInButtonPressed()
{
    float horizontalRatio = getScrollBarRatio(ui->scrollArea->horizontalScrollBar());
    float verticalRatio = getScrollBarRatio(ui->scrollArea->verticalScrollBar());
    imageContainer->zoomIn(0.1);
    ui->scrollArea->horizontalScrollBar()->setValue(ui->scrollArea->horizontalScrollBar()->maximum()*horizontalRatio);
    ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->maximum()*verticalRatio);
}

void MainWindow::zoomOutButtonPressed()
{
    float horizontalRatio = getScrollBarRatio(ui->scrollArea->horizontalScrollBar());
    float verticalRatio = getScrollBarRatio(ui->scrollArea->verticalScrollBar());
    imageContainer->zoomOut(0.1);
    ui->scrollArea->horizontalScrollBar()->setValue(ui->scrollArea->horizontalScrollBar()->maximum()*horizontalRatio);
    ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->maximum()*verticalRatio);
}

void MainWindow::zoomOriginalButtonPressed()
{
    imageContainer->setOriginalSize();
}

void MainWindow::upateImageInfoWidget()
{
    imageInfo->setImageName(image->getName());
    imageInfo->setImageIndex(imageList->currentRow()+1);
    imageInfo->setImageListSize(imageList->count());
    imageInfo->setImageSize(image->getImageSize());
    imageInfo->setFileSize(image->getFileSize());
    imageInfo->setFileCreate(image->created());
    imageInfo->showInfo();

}

void MainWindow::enableButtons()
{
    prevButton->setEnabled(true);
    nextButton->setEnabled(true);
    zoomInButton->setEnabled(true);
    zoomOutButton->setEnabled(true);
    fitToWindowButton->setEnabled(true);
    zoomOriginalButton->setEnabled(true);

    removeImageAction->setEnabled(true);
    setAsWallpaperXFCEAction->setEnabled(true);
}

void MainWindow::disableButtons()
{
    prevButton->setDisabled(true);
    nextButton->setDisabled(true);
    zoomInButton->setDisabled(true);
    zoomOutButton->setDisabled(true);
    fitToWindowButton->setDisabled(true);
    zoomOriginalButton->setDisabled(true);

    removeImageAction->setDisabled(true);
    setAsWallpaperXFCEAction->setDisabled(true);
}

void MainWindow::imageListRowChanged(int row)
{
    if(row > -1)
    {
        if(image->load(imageList->currentImage()))
        {
            upateImageInfoWidget();
            imageContainer->setImage(image->getImage());
        }
    }else{
        imageContainer->clear();
        imageInfo->hideInfo();
        this->disableButtons();
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Plus:
        zoomInButtonPressed();
        break;
    case Qt::Key_Minus:
        zoomOutButtonPressed();
        break;
    case Qt::Key_X:
        imageList->next();
        break;
    case Qt::Key_Z:
        imageList->previous();
        break;
    case Qt::Key_Delete:
        removeImageActionTriggered();
        break;
    default:
        break;
    }
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
    
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    imageContainer->fitImageToParent();
}
