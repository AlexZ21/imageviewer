#ifndef XFCEWALLPAPERSETTER_H
#define XFCEWALLPAPERSETTER_H

#include <QObject>
#include <QWidget>
#include <QDir>
#include <QFile>
#include <QDomDocument>
#include <QDesktopWidget>
#include <QDebug>
#include <QProcess>


class XFCEWallpaperSetter : public QObject
{
    Q_OBJECT
    QDesktopWidget *descWidget;
public:
    explicit XFCEWallpaperSetter(QDesktopWidget *descWidget, QObject *parent = 0);

    void setAsWallpaper(QString file);

signals:

public slots:

};

#endif // XFCEWALLPAPERSETTER_H
