#include "xfcewallpapersetter.h"

#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

XFCEWallpaperSetter::XFCEWallpaperSetter(QDesktopWidget *descWidget, QObject *parent) :
    QObject(parent)
{
    this->descWidget = descWidget;
}

void XFCEWallpaperSetter::setAsWallpaper(QString file)
{
    Display *dpy;
    XRRScreenResources *screen;
    dpy = XOpenDisplay(NULL);
    screen = XRRGetScreenResources (dpy, DefaultRootWindow(dpy));

    for (int j = 0; j < screen->noutput; j++)
    {
        XRROutputInfo *outputInfo = XRRGetOutputInfo(dpy, screen, screen->outputs[j]);
        XRRCrtcInfo *crtcInfo = NULL;
        crtcInfo= XRRGetCrtcInfo(dpy, screen, outputInfo->crtc);
        if(crtcInfo)
        {
            QStringList arg;
            arg << "-c" << "xfce4-desktop" << "-p" <<
                   "/backdrop/screen"+QString::number(0)+"/monitor"+QString(outputInfo->name)+"/workspace0/last-image" <<
                   "-n" << "-t" << "string" << "-s" << file;
            QProcess *proc = new QProcess(this);
            proc->start("xfconf-query", arg);
            proc->waitForFinished();
        }
        XRRFreeCrtcInfo(crtcInfo);
        XRRFreeOutputInfo(outputInfo);

    }

    XRRFreeScreenResources(screen);
}
