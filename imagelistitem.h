#ifndef IMAGELISTITEM_H
#define IMAGELISTITEM_H

#include <QObject>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QFontMetrics>
#include <QFont>
#include <QBrush>
#include <QPen>
#include <QDateTime>

class ImageListItem : public QStyledItemDelegate
{
public:
    ImageListItem(QObject *parent=0) : QStyledItemDelegate (parent){}

    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const{

        painter->setRenderHint(QPainter::Antialiasing, true);

        if(option.state & QStyle::State_Selected){
            painter->fillRect(option.rect, option.palette.color(QPalette::Highlight));
        }


        QString rowCount = QString::number(index.model()->rowCount());
        QString title = index.data(Qt::DisplayRole).toString();
        QString number = QString::number(index.row()+1);//index.data(Qt::UserRole).toString();
        int size = index.data(Qt::UserRole).toInt();

        QString date = QDateTime::fromMSecsSinceEpoch(index.data(Qt::UserRole+2).toLongLong()).toString("dd.MM.yyyy");


        QFont numberFont;
        numberFont.setPointSize(10);
        numberFont.setWeight(QFont::Bold);

        QBrush numberRectBackground(QColor(220,220,220));
        painter->setBrush(numberRectBackground);
        QPen numberRectBackgroundBorder(QColor(0,0,0,0));
        painter->setPen(numberRectBackgroundBorder);

        QFontMetrics numberFontMetric(numberFont);

        QRect numberRect(option.rect.x()+6, option.rect.y()+12, numberFontMetric.width(rowCount)+10,26);
        painter->drawRoundedRect(numberRect,2,2);

        QPen numberText(QColor(0,0,0,255));
        painter->setPen(numberText);

        painter->setFont(numberFont);
        painter->drawText(numberRect, Qt::AlignCenter, number, &numberRect);

        QFont titleFont;
        titleFont.setWeight(QFont::DemiBold);
        painter->setFont(titleFont);
        QRect titleRect(numberFontMetric.width(rowCount)+24, option.rect.y()+8, option.rect.width()-numberRect.width()+6,16);
        painter->drawText(titleRect, Qt::AlignLeft | Qt::AlignVCenter, title, &titleRect);

        QFont infoFont;
        painter->setFont(infoFont);
        QRect infoRect(numberFontMetric.width(rowCount)+24, option.rect.y()+26, option.rect.width()-numberRect.width()+6,16);
        painter->drawText(infoRect, Qt::AlignLeft | Qt::AlignVCenter, QString::number(size) +
                          QString(tr("Кб")) + ", " + date, &infoRect);


    }

    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const{
        return QSize(240, 50);
    }
};

#endif // IMAGELISTITEM_H
