#include "image.h"

Image::Image(QObject *parent) :
    QObject(parent)
{
}

bool Image::load(QString path)
{
    if(image.load(path))
    {
        imageInfo.setFile(path);
        return true;
    }else{
        return false;
    }
}

QImage *Image::getImage()
{
    return &image;
}

QDir Image::getDirectory()
{
    return imageInfo.dir();
}

QString Image::getName()
{
    return imageInfo.fileName();
}

QSize Image::getImageSize()
{
    return image.size();
}

int Image::getFileSize()
{
    return imageInfo.size()/1000;
}

QString Image::created()
{
    return imageInfo.created().toString("dd.MM.yyyy");
}

QString Image::getPath()
{
    return imageInfo.filePath();
}
