#include "mainwindow.h"
#include <QApplication>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    qDebug() << a.arguments();
//    QFile file("log.txt");
//    file.open(QIODevice::WriteOnly);
//    QTextStream out(&file);
//    foreach (QString str, a.arguments()) {
//       out << str << "\n";
//    }

//    file.close();


    MainWindow w(a.desktop());

    if(!a.arguments().value(1).isEmpty())
    {
        w.openImage(a.arguments().value(1));
    }

    w.show();

    return a.exec();
}
