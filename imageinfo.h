#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <QWidget>
#include <QLabel>


namespace Ui {
class ImageInfo;
}

class ImageInfo : public QWidget
{
    Q_OBJECT
public:
    explicit ImageInfo(QWidget *parent = 0);
    ~ImageInfo();

    void setImageName(QString name);
    void setImageIndex(int index);
    void setImageListSize(int size);
    void setImageSize(QSize size);
    void setFileSize(int size);
    void setFileCreate(QString date);


    void hideInfo();
    void showInfo();

private:
    Ui::ImageInfo *ui;
};

#endif // IMAGEINFO_H
