#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QToolButton>
#include <QAction>
#include <QActionGroup>
#include <QMenu>
#include <QIcon>
#include <QFileDialog>
#include <QScrollBar>
#include <QKeyEvent>
#include <QMessageBox>

#include <imageinfo.h>
#include "imagecontainer.h"
#include "image.h"
#include "imagelistitem.h"
#include "imagelist.h"
#include "xfcewallpapersetter.h"

#include <QDebug>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QDesktopWidget *descWidget;

    //Toolbar buttons
    QToolButton *nextButton;
    QToolButton *prevButton;
    QToolButton *fitToWindowButton;
    QToolButton *zoomOriginalButton;
    QToolButton *zoomInButton;
    QToolButton *zoomOutButton;
    QToolButton *settingsButton;
    void createToolButtons();


    //Actions
    QAction *openImageAction;
    QAction *removeImageAction;
    QAction *setAsWallpaperXFCEAction;
    QAction *showFileListAction;
    QAction *aboutAction;
    QAction *exitAction;
    QActionGroup *sortActionGroup;
    QActionGroup *directionSortActionGroup;
    QAction *nameSortAction;
    QAction *dateSortAction;
    QAction *sizeSortAction;
    QAction *typeSortAction;
    QAction *upSortAction;
    QAction *downSortAction;
    void createActions();


    //My classes :)
    ImageInfo *imageInfo;
    ImageContainer *imageContainer;
    Image *image;
    ImageList *imageList;




    float getScrollBarRatio(QScrollBar *scrollBar);

    void upateImageInfoWidget();

    void enableButtons();
    void disableButtons();

public slots:
    void openImageActionTriggered();
    void removeImageActionTriggered();
    void setAsWallpaperXFCEActionTriggered();
    void aboutActionTriggered();

    void sortActionGroupTriggered(QAction *action);
    void directionSortActionGroupTriggered(QAction *action);

    void fitToWindowButtonToggled(bool mode);
    void zoomInButtonPressed();
    void zoomOutButtonPressed();
    void zoomOriginalButtonPressed();

    void imageListRowChanged(int row);
public:
    explicit MainWindow(QDesktopWidget *descWidget, QWidget *parent = 0);
    ~MainWindow();

    void openImage(QString filePath);

private:
    Ui::MainWindow *ui;

protected:
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);
};

#endif // MAINWINDOW_H
