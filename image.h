#ifndef IMAGE_H
#define IMAGE_H

#include <QObject>
#include <QImage>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>

#include <QDebug>

class Image : public QObject
{
    Q_OBJECT
    QImage image;
    QFileInfo imageInfo;

public:
    explicit Image(QObject *parent = 0);
    bool load(QString path);

    QImage *getImage();
    QDir getDirectory();

    QString getName();
    QSize getImageSize();
    int getFileSize();
    QString created();
    QString getPath();
signals:

public slots:

};

#endif // IMAGE_H
