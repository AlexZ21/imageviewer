#ifndef IMAGECONTAINER_H
#define IMAGECONTAINER_H

#include <QWidget>
#include <QImage>
#include <QPainter>
#include <QDebug>
#include <QTimer>

class ImageContainer : public QWidget
{
    Q_OBJECT
    QImage *image;
    QSize originalSize;
    QTimer fadeTimer;
    float zoomFactor;
    float alpha;
    bool fitToParent;
public:
    explicit ImageContainer(QWidget *parent = 0);
    void setImage(QImage *im);

    void zoomIn(float factor);
    void zoomOut(float factor);
    void setOriginalSize();
    float getZoomFactor();

    void clear();

    bool getFitToParent() const;
    void setFitToParent(bool value);
    void fitImageToParent();
signals:

public slots:
    void fadeIn();

protected:
    void paintEvent(QPaintEvent *event);

};

#endif // IMAGECONTAINER_H
