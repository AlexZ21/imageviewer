#ifndef IMAGELIST_H
#define IMAGELIST_H

#include <QListWidget>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QList>
#include <QDateTime>
#include <QtAlgorithms>
#include <QScrollBar>

#include <QDebug>

#include <QPainter>
#include <QImage>

#include <QThreadPool>
#include <QRunnable>


#include "imagelistitem.h"

class ImageList;
class SortTask;
class LoadImagesTask;

enum ILSortType{
    ILNameSort,
    ILDateSort,
    ILSizeSort,
    ILTypeSort
};

enum ILSortDir{
    ILUpSort,
    ILDownSort
};

class PtrLess // public std::binary_function<bool, const T*, const T*>
{
public:
  ILSortType type;
  ILSortDir dir;

  PtrLess(ILSortType type, ILSortDir dir)
  {
      this->type = type;
      this->dir = dir;
  }

  bool operator()(const QListWidgetItem* a, const QListWidgetItem* b) const
  {
    if(type == ILSizeSort)
    {
        if(dir == ILUpSort)
            return a->data(Qt::UserRole).toInt() < b->data(Qt::UserRole).toInt();
        else
            return a->data(Qt::UserRole).toInt() > b->data(Qt::UserRole).toInt();
    }
    if(type == ILNameSort)
    {
        int res = QString::localeAwareCompare (a->text(), b->text());

        if(dir == ILUpSort)

            if(res >= 0)
                return false;
            else return true;

        else
            if(res >= 0)
                return true;
            else return false;
    }

    if(type == ILDateSort)
    {
        if(dir == ILUpSort)
            return a->data(Qt::UserRole+2).toLongLong() < b->data(Qt::UserRole+2).toLongLong();
        else
            return a->data(Qt::UserRole+2).toLongLong() > b->data(Qt::UserRole+2).toLongLong();
    }

    if(type == ILTypeSort)
    {
        int res = QString::localeAwareCompare (a->data(Qt::UserRole+3).toString(), b->data(Qt::UserRole+3).toString());

        if(dir == ILUpSort)
            if(res >= 0)
                return false;
            else return true;

        else
            if(res >= 0)
                return true;
            else return false;
    }

    return false;
  }
};

class ImageList : public QListWidget
{
    Q_OBJECT


    QImage loadCircleImage;
public:
    explicit ImageList(QWidget *parent = 0);

    void setDirectory(QDir directory, QString imageName);
    void setCurrentImageByName(QString imageName);
    void setCurrentImageByIndex(int index);

    QString currentImage();
    void removeSelectedItems();

    void setSortType(ILSortType type);
    void setSortDir(ILSortDir dir);
    void updateSort();

    QDir directory;
    bool sortProcess;
    bool loadProcess;
    ILSortType sortType;
    ILSortDir sortDir;

protected:
    void paintEvent(QPaintEvent *e);


signals:

public slots:
    void next();
    void previous();
};


class SortTask : public QRunnable
{
    ImageList *imageList;
public:
    SortTask(ImageList *imageList)
    {
        this->imageList = imageList;
    }

    void run()
    {
        imageList->loadProcess = true;
        imageList->updateSort();
        imageList->loadProcess = false;
    }
};

class LoadImagesTask : public QRunnable
{
    ImageList *imageList;
    QString imageName;
public:
    LoadImagesTask(ImageList *imageList, QString imageName)
    {
        this->imageList = imageList;
        this->imageName = imageName;

    }

    void run()
    {
        imageList->loadProcess = true;

        QList<QListWidgetItem *> it;

        foreach (QFileInfo file, imageList->directory.entryInfoList()) {
            QListWidgetItem *item = new QListWidgetItem();
            item->setText(file.fileName());
            item->setData(Qt::UserRole, int(file.size()/1000));
            item->setData(Qt::UserRole+1, file.filePath());
            item->setData(Qt::UserRole+2, file.created().toMSecsSinceEpoch());
            item->setData(Qt::UserRole+3, file.suffix());
            it.push_back(item);
        }

        qSort(it.begin(), it.end(), PtrLess(imageList->sortType, imageList->sortDir));

        for(int i = 0; i < it.size(); ++i)
            imageList->addItem(it[i]);

        imageList->setCurrentImageByName(imageName);
        imageList->loadProcess = false;
    }
};
#endif // IMAGELIST_H
