#include "imagecontainer.h"



ImageContainer::ImageContainer(QWidget *parent) :
    QWidget(parent)
{
    this->image = NULL;
    this->zoomFactor = 1.0;
    this->alpha = 0;
    this->fitToParent = true;
    QObject::connect(&this->fadeTimer, SIGNAL(timeout()), this, SLOT(fadeIn()));
}

void ImageContainer::setImage(QImage *im)
{
    zoomFactor = 1.0;
    alpha = 1;
    this->image = im;
    originalSize = image->size();
    this->resize(image->size());
    if(fitToParent)
    {
        fitImageToParent();
    }

   //fadeIn();
}

void ImageContainer::zoomIn(float factor)
{
    zoomFactor += factor;
    this->resize(image->size()*zoomFactor);
}

void ImageContainer::zoomOut(float factor)
{
    zoomFactor -= factor;
    this->resize(image->size()*zoomFactor);
}

void ImageContainer::setOriginalSize()
{
    this->resize(originalSize);
    zoomFactor = 1.0;
}

float ImageContainer::getZoomFactor()
{
    return zoomFactor;
}

void ImageContainer::clear()
{
    image = NULL;
    repaint();
}

bool ImageContainer::getFitToParent() const
{
    return fitToParent;
}

void ImageContainer::setFitToParent(bool value)
{
    fitToParent = value;
    fitImageToParent();
}

void ImageContainer::fitImageToParent()
{
    if(fitToParent)
    {
        if(image) this->resize(image->size());
        if(this->height() < this->parentWidget()->height() && this->width() < this->parentWidget()->width())
        {
            this->setOriginalSize();
        }else if(this->height() > this->parentWidget()->height() && this->width() < this->parentWidget()->width()){
            float ratio = (float)this->width()/this->height();
            this->resize(this->parentWidget()->height()*ratio, this->parentWidget()->height());
        }else if(this->height() < this->parentWidget()->height() && this->width() > this->parentWidget()->width()){
            float ratio = (float)this->height()/this->width();
            this->resize(this->parentWidget()->width(), this->parentWidget()->width()*ratio);
        }else{
            if(this->height()>this->width())
            {
                float ratio = (float)this->width()/this->height();
                this->resize(this->parentWidget()->height()*ratio, this->parentWidget()->height());
            }else{
                float ratio = (float)this->height()/this->width();

                if(this->parentWidget()->width()*ratio > this->parentWidget()->height()){
                    this->resize(this->parentWidget()->height()/ratio, this->parentWidget()->height());
                }else{
                    this->resize(this->parentWidget()->width(), this->parentWidget()->width()*ratio);
                }


            }
        }
    }
}


void ImageContainer::fadeIn()
{
    this->alpha+=0.1;
    repaint();
    if(this->alpha < 1.0)
        this->fadeTimer.start(15);
}

void ImageContainer::paintEvent(QPaintEvent *event)
{
    if(image)
    {
        QPainter painter(this);
        painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
        painter.setOpacity(alpha);
        painter.drawImage(QRect(QPoint(),this->size()), *image);

    }else{

    }
}
