#include "imageinfo.h"
#include "ui_imageinfo.h"

ImageInfo::ImageInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ImageInfo)
{
    ui->setupUi(this);
    this->hideInfo();
}

ImageInfo::~ImageInfo()
{
    delete ui;
}

void ImageInfo::setImageName(QString name)
{
    ui->fileName->setText(name+" - ");
}

void ImageInfo::setImageIndex(int index)
{
    ui->currentImageId->setText(QString::number(index));
}

void ImageInfo::setImageListSize(int size)
{
    ui->imageListSize->setText(QString::number(size));
}

void ImageInfo::setImageSize(QSize size)
{
    ui->imageSize->setText(QString::number(size.width())+"x"+QString::number(size.height())+", ");
}

void ImageInfo::setFileSize(int size)
{
    ui->fileSize->setText(QString::number(size)+"Кб, ");
}

void ImageInfo::setFileCreate(QString date)
{
    ui->fileCreated->setText(date);
}

void ImageInfo::hideInfo()
{
    ui->fileName->hide();
    ui->currentImageId->hide();
    ui->label->hide();
    ui->imageListSize->hide();
    ui->imageSize->hide();
    ui->fileSize->hide();
    ui->fileCreated->hide();
}

void ImageInfo::showInfo()
{
    ui->fileName->show();
    ui->currentImageId->show();
    ui->label->show();
    ui->imageListSize->show();
    ui->imageSize->show();
    ui->fileSize->show();
    ui->fileCreated->show();
}

